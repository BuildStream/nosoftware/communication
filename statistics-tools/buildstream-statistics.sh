#!/bin/bash

. common.sh

remote=https://gitlab.com/BuildStream/buildstream.git

git clone "${remote}" "${tmpdir}"

cd "${tmpdir}"

CLOCK_OPTIONS="--exclude-dir=_protos"

scan_commits buildstream:code doc tests
