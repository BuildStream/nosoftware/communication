#!/bin/bash

. common.sh

remote=https://gitlab.com/BuildStream/website.git

git clone "${remote}" "${tmpdir}"

cd "${tmpdir}"

CLOCK_OPTIONS="--exclude-dir='.*theme'"

scan_commits .
