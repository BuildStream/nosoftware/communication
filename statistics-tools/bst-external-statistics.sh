#!/bin/bash

. common.sh

remote=https://gitlab.com/BuildStream/bst-external.git

git clone "${remote}" "${tmpdir}"

cd "${tmpdir}"

CLOCK_OPTIONS=""

scan_commits bst_external:code doc tests
