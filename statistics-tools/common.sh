set -eu

tmpdir="$(mktemp --tmpdir -d git-stat.XXXXXXXXXX)"

cleanup() {
    rm -rf "${tmpdir}"
}

trap cleanup EXIT

clocdir() {
    files=0
    blank=0
    comment=0
    code=0
    if ! [ -d "${1}" ]; then
       echo -n "0,0,0,0"
       return
    fi
    for language in $(cloc ${CLOCK_OPTIONS} --quiet --csv "${1}" | sed "1d;2d" | cut -d, -f1,3-); do
       files="$(("${files}"+"$(echo "${language}" | cut -d, -f1)"))"
       blank="$(("${blank}"+"$(echo "${language}" | cut -d, -f2)"))"
       comment="$(("${comment}"+"$(echo "${language}" | cut -d, -f3)"))"
       code="$(("${code}"+"$(echo "${language}" | cut -d, -f4)"))"
    done
    echo -n "${files},${blank},${comment},${code}"
}

scan_commits () {
    echo -n "commit,date,authors"
    if [ "$#" -lt 2 ]; then
       echo ",files,blank,comment,code"
    else
	for dir in "$@"; do
	    dir="$(echo "${dir}" | sed 's/.*://')"
            echo -n ",${dir}-files,${dir}-blank,${dir}-comment,${dir}-code"
	done
	echo ""
    fi
    for commit in $(git log --first-parent --format=%H); do
       git checkout "${commit}"

       case "$(git log -1 --format=%P "${commit}" | wc -w)" in
           1)
              commits="-1 ${commit}^"
              ;;
           2)
              base="$(git merge-base "${commit}^" "${commit}^2")"
              commits="${base}..${commit}^2"
              ;;
           *)
              echo "Unexpected number of parents for ${commit}" 1>&2
              exit 1
              ;;
       esac
       authors="$(git log --format="%ae" ${commits} | sort -u | sed ':l;N;s/\n/:/;bl')"
       date="$(git log -1 --format="%ct" "${commit}")"
       echo -n "${commit},${date},${authors}"
       for dir in "$@"; do
	   dir="$(echo "${dir}" | sed 's/:.*//')"
	   echo -n ","
           clocdir "${dir}"
       done
       echo ""
    done
}
