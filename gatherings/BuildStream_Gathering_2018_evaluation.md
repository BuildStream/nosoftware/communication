## BuildStream Gathering 2018: evaluation

Evaluation of the BuildStream Gathering 2018 that took place from Tuesday to Friday on week 42 (October) at Codethink offices, in Manchester, UK.

The survey was created by Agustin and reviewed by Laurence using GDoc. In order to keep anonymity, the raw data will not be disclosed. It would be obvious to identify several people.

This evaluation has been done by Agustin, reflecting all/most relevant feedback. 

There was a mistake in one of the Logistics questions. The answer scale lacked a value "4". It was corrected after several people already filled out the survey. It had little impact on the result though.

### Survey summary

## General feedback

* Participation: 
   * 12 people filled out the form.
   * 3 participants on the training sessions answered the survey.
   * Agustin B.B., as organizer, did not complete the survey.

The following data refers to a scale from 1 to 5 where 1 is the minimum value.

* How satisfied were you with the event in general?
   * Avg(*): 3.5
   * Median: 4
   * Std Dev(**): 1.38

#### Logistics
* [Location]
   * Avg: 4.18
   * Median: 5
   * Std Dev: 1.08
* [Room / venue]
   * Avg: 3.67
   * Median: 4
   * Std Dev: 1.37
* [Information prior to the event]
   * Avg: 3.09
   * Median: 3
   * Std Dev: 1.22
* [Restaurant / food / beverages]
   * Avg: 4.38
   * Median: 5
   * Std Dev: 1.19
* [Participation]
   * Avg: 3.82
   * Median: 4
   * Std Dev: 1.08
* [Moderation]
   * Avg: 3.2
   * Median: 3
   * Std Dev: 1.55

Additional comments:
* There are several people unhappy with the schedule changes or the fact that in some cases the Gathering started a few minutes ahead of schedule in the morning.
* There are a couple of comments about organising the event out of Codethink offices, in Manchester or London, or organise parallel tracks for a variety of reasons like the room being small for the number of people present, the impact of the Gathering on those people working at Codethink's office or for convenience.
* There is one explicit comment reflecting satisfaction. 

#### Agenda

* What is your overall evaluation of the agenda?
   * Avg: 2.92
   * Median: 3
   * Std Dev: 1.16

* Name the most relevant topics or sessions (3 choices):
   * Testing (5)
   * Docker / OCI (3)
   * BuildStream 1.4 features list (plan) and schedule (3)
   * BuildStream Architecture (3)
   * Hacking sessions (2)
   * SourceCache (2)
   * Workspaces (2)
   * Several others got one mention.

Additional comments:
* Three mentions/complains about some agenda topics starting earlier that schedule in the morning or changed against plan which led to some people missing them.
* There is a suggestion on having the agenda set up earlier.
* There is a positive mention on having the agenda before hand and people had a chance to participate on it.  


#### Training sessions

* BuildStream 101
   * Avg: 4
   * Median: 4
   * Std Dev: 0

Comments:
* A suggestion to improve the slides on the CLI commands to run.
* A positive comment about the session.

* BuildStream 102
   * Avg: 3.67
   * Median: 4
   * Std Dev: 0.58

Comments:
* Suggestion of having the training sessions earlier in the day.

#### Outcome

* Please describe which outcome would you highlight out of this Gathering.
   * Personal interaction (expressed in different ways): (3)
   * Agreed 1.4 feature plan list and release schedule (2)
   * Architecture (2)
   * Mission statement (2)
   * Testing
   * Disruptions to others at the office.
   * Any of the output had much benefit

Additional comments and suggestions:
* Stick to the agenda
* Hold the event elsewhere
* Have a backlog of short discussion topics in case a session or the agenda scheduling finishes earlier, so we stick to the plan. 
* Starting at 9 on Tuesday was not helpful.
* Introducing the hacking sessions was good.
* A good event overall.
* Productive event.

### Organizer's evaluation and recommendations

#### Participation

* We had peaks at the Gathering of over 20 people so there is a significant number that did not fill out the survey. 
   * The survey was promoted during the last day of the Gathering. A reminder was sent to the mailing list and it was mentioned in IRC channels. More reminders might have slightly increased the number of answers. 
   * With a completed participants list, it would have been possible to send individual reminders about the survey automatically.
* A subset of the training sessions participants might not be subscribed to BuildStream channels so they weren't aware of the survey.
   * The participants list for the training sessions and the Gathering should have been placed in the same list instead of in different locations (wiki and tickets).

#### Logistics

a.- During some sessions on Thursday and Friday we had more people than the big room can afford during the general topics sessions.
* Organising the Gathering out of Codethink offices was never on the table. I realized the week before the Gathering some sessions on Thursday and Friday would exceed the room capacity. The following mitigation measures were taken:
    * The breaks lengths between sessions were extended.
    * An additional room was booked for the hacking sessions. 
    * The schedule helped to make the situation affordable since All Hands sessions were never longer than 90-100 minutes including breaks.
* With the current amount of people either we move into a parallel tracks model or we look for a place with a bigger room for All Hands sessions and a couple of additional small rooms. 
    * Finding a place in Manchester that fills all requirements (one big room, a couple of small ones, close to Codethink's office and low cost) would be ideal although it would significantly increase the organization effort. 
    * Moving the Gathering out of the Codethink offices might have an impact on the number of participants. This needs to be evaluated in advance.

b.- Info prior to the event
* We have now the dates for the next Gatherings. This is an improvement.
* It would be good to confirm the schedule and location of the next gathering on each gathering.
* We can have more information about the training sessions in advance, now that we know the dates of the next Gathering and we can use this first experience as example.
* More effort should be made in having the material related with any presentation in advance, so people can go over it before the activity takes place.
   * The goal should be to publish the presentations as a video together with the slides/documentation to the wider community a few days after the event.
* Having the participants in a list with their mails in advance solve several issues:
   * Improve the management of the restaurants booking.
   * Allows to scope better the rooms capacity and evaluate potential mitigation measures in advance.
   * Allows to automate the process of sending and remind to fill out the survey.

c.- Dietary restrictions
* There were participants with dietary restrictions.
* This should be considered in the next Gathering. I did not on this one. Apologies.
   * Include a column on the participants list asking for dietary restrictions.

d.- The moderators role can and should rotate during the Gathering. As we had in this Gathering, a second person is desired.
* Laurence help was much appreciated. The event is too big/long for a single person to manage.

#### Agenda

a.- Agenda creation and management
* I am happy with the level of participation.
* I consider a success arriving to the Gathering with a good draft.
* I expected more changes on the agenda than there were, which gave us some additional time in the mornings and made scheduling significantly easier. 
   * This was the thinking behind starting the first day at 9 instead of 9:30.
* We can work on the agenda even earlier, now that we know the dates of the next Gathering several months in advance. Closing the venue is a pre-requisite though.
* One of the key demands from participants in previous events were the availability of none structured slots. I think we reached a good balance between structured and unstructured sessions.
   * Having the unstructured sessions before and after lunch has worked well for me in several occasions. 
* Using a specialized tool or a calendar tool helps in:
   * Designing the agenda
   * Managing changes during the event and notifying them.

#### Other topics

* I was happy to see some activities being recorded. Some of them will be published as soon as possible.
* I think organising training sessions was a good decision. We can build up from this experience.
   * Thank you to those who made it possible.
   * They were scheduled at the end of the day so Codethings that were not involved in BuildStream could attend. The same applied to the Architecture session. I think it was a good decision.
* Codethink support was essential to keep the management effort under control.
   * I suggest to keep organising the Gatherings at Codethink or any other stakeholder offices while the number of participants afford it.
   * A different organizer/moderator for the next Gathering should be appointed. Any volunteer is welcome. 
* I think the complaints related with the agenda changes and notification of those changes have little basis when taking a global look at the event.
   * As mentioned through IRC, I took the conscious decision to favor the desires of those present in the room over those who were elsewhere, with a only couple of exceptions.
   * The agenda was designed to be flexible. It was communicated in this way.
   * Several measures were taken to radiate changes live: the calendar, so those subscribed to it could receive notifications, IRC were many of the changes were announced... 
      * More can always be done in this regard.
   * I insist on how useful is the usage of a calendar or and event management tool. Being subscribed to the interesting sessions allow you to get automatic notifications of changes.
   * In summary, I am fairly happy with how things went in this regard.
* I understand and share the concerns about the capacity of the room. It was too packed during some sessions on Thursday and Friday.
   * The solution to this issue in the future will not simple without a significant increase of management effort and costs (moving to a different venue) or the assumption that we will not have all hands sessions (staying at Codethink offices).
* I am particularly happy that so many people came from London and that we could accommodate most of their topics on the agenda.
   * Knowing the dates of the next Gathering as we do now should make their participation easier.

Given the amount of effort put in the organization of the event, the level of participation and its outcome, I think that, assuming there is plenty of room for improvements, the results were satisfactory, in my opinion, specially when compared with the previous event in Almeria, Spain.

Further information:
* Tickets related with the organization of the Gathering: https://gitlab.com/BuildStream/nosoftware/communication/boards/852013?milestone_title=ELCE-BuildStream_Gathering_2018&
* Wiki page dedicated to the event: https://wiki.gnome.org/Projects/BuildStream/Events#BuildStream_Gathering_2018
* Event calendar (week 42): https://calendar.google.com/calendar/ical/codethink.co.uk_mpgah0uj538hnbsf4l7b4rchts%40group.calendar.google.com/public/basic.ics
* Minutes: https://gitlab.com/BuildStream/nosoftware/communication/blob/master/gatherings/BuildStream_Gathering_2018_minutes.md

(*) Avg expressed as arithmetic mean: http://www.differencebetween.net/science/difference-between-average-and-mean/
(**) Std Dev: https://en.wikipedia.org/wiki/Standard_deviation

Thanks Codethink for sponsoring the Gathering and the support in organization tasks.
